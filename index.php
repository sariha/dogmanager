<?php

//
require 'class/Dog.Class.php';
require 'class/Chihuahua.Class.php';
require 'class/Bernese.Class.php';


?><!DOCTYPE html>
<html>
<head>
    <title>DogManager</title>


    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.4.0/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.4.0/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>

</head>

<body>

<h1>DogManager usage example</h1>

<h2>Documentation</h2>
<p>
    <a href="html/hierarchy.html">see documentation</a>
</p>
<?php

//usage :
$kiki01 = new Chihuahua(array(
    'hairColor'     =>  "#755014",
    'dogName'       =>  'Kiki 01',
    'isBarking'     =>  false
));

//feed the dog
$kiki01->feed();

//walk the dog
if($kiki01->hasBeenWalked() === false ) {
    $kiki01->walk();
}

echo '<h2>Chihuahua</h2>';

?>
<pre>
    <code class="php">
    //usage :
    $kiki01 = new Chihuahua(array(
        'hairColor'     =>  "#755014",
        'dogName'       =>  'Kiki 01',
        'isBarking'     =>  false
    ));

    //feed the dog
    $kiki01->feed();

    //walk the dog
    if($kiki01->hasBeenWalked() === false ) {
        $kiki01->walk();
    }
    </code>
</pre>

result :
<?php
echo '<pre><code class="php">';
var_dump($kiki01);
echo '</code></pre>';

//set initial state :
$bernese01 = new Bernese(array(
    'hairColor'  => '#D17A17',
    'dogName'   => 'Cookie',
    'sleepWithACushion' => true,
    'hasBeenWalked' => true,
    'isHungry' => false
));

//play with cookie
$bernese01->playWith();

echo '<h2>Bernese</h2>';
?>
<pre>
    <code class="php">
    //set initial state :
    $bernese01 = new Bernese(array(
        'hairColor'  => '#D17A17',
        'dogName'   => 'Cookie',
        'sleepWithACushion' => true,
        'hasBeenWalked' => true,
        'isHungry' => false
    ));

    //play with cookie
    $bernese01->playWith();
    </code>
</pre>
<?php
echo '<pre><code class="php">';
var_dump($bernese01);
echo '</code></pre>';


?>
</body>
</html>

