var searchData=
[
  ['_24bernese01',['$bernese01',['../index_8php.html#a043730535b9775016ff2cf017f8ade7f',1,'index.php']]],
  ['_24dogname',['$dogName',['../class_dog.html#a2ae8b89e1b3d3e4ce6292b8fa30f4087',1,'Dog']]],
  ['_24haircolor',['$hairColor',['../class_dog.html#a0288ffb8304f5f91bbd7d3db35ed9298',1,'Dog']]],
  ['_24hasbeenwalked',['$hasBeenWalked',['../class_dog.html#a2fb9a8d73d48c72a1bcda683ea330303',1,'Dog']]],
  ['_24hasstraighthears',['$hasStraightHears',['../class_chihuahua.html#a9509a347548516fb97effd044df8340f',1,'Chihuahua']]],
  ['_24isbarking',['$isBarking',['../class_dog.html#af54825f5d7fcdb089285b668ff882c01',1,'Dog']]],
  ['_24ishungry',['$isHungry',['../class_dog.html#ad526fe57b6f799a298e90cfe62a8a910',1,'Dog']]],
  ['_24kiki01',['$kiki01',['../index_8php.html#a7fe3a613209a774eb095ea8d8a2cda10',1,'index.php']]],
  ['_24liketobegroomed',['$likeToBeGroomed',['../class_bernese.html#ab34e09a42ec4cbf2678b522b7af288b8',1,'Bernese']]],
  ['_24sleepwithacushion',['$sleepWithACushion',['../class_bernese.html#a8b5d671689e886f1e9eeca4387700f34',1,'Bernese']]],
  ['_24tailcolor',['$tailColor',['../class_bernese.html#a6149086d78abef4f1cdd28c724be869c',1,'Bernese']]]
];
