<?php

    /**
     * Class Dog
     *
     * Base Class for DogManager
     *
     */
    Abstract class Dog {

        /**
         * Hair Color
         * @var string (hex color code)
         */
        public    $hairColor = '#755014';

        /**
         * Dog Name
         * @var string
         */
        public    $dogName   = '';

        /**
         * is Barking
         * @var bool
         */
        protected $isBarking = false;

        /**
         * is Hungry
         * @var bool
         */
        protected $isHungry  = true;

        /**
         * has been walked ?
         * @var bool
         */
        protected $hasBeenWalked = false;


        /**
         * Dog constructor.
         * @param array $params
         */
        public function __construct($params = array()) {

            //parameters are in an array
            foreach ($params as $param_name => $param_value) {
                $this->$param_name = $param_value;
            }

        }


        /**
         * Is Dog Barking ?
         * @return bool
         */
        public function isBarking() {

            return $this->isBarking;
        }

        /**
         * Is Dog Hungry ?
         * @return bool
         */
        public function isHungry() {

            return $this->isHungry;

        }


        /**
         * Feed the dog, and set isHungry to false.
         * return false if dod is not hungry yet
         * @return bool
         */
        public function feed() {

            if( $this->isHungry() === true ) {
                $this->isHungry = false;
                return true;
            } else {
                return false;
            }

        }


        /**
         * @return bool
         */
        public function hasBeenWalked() {
            return $this->hasBeenWalked;
        }

        /**
         * Walk the dog
         */
        public function walk() {
            $this->hasBeenWalked = true;
        }

    }