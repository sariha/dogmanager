<?php

    /**
     * Class Bernese
     */
    class Bernese extends Dog {

        /**
         * Tail color
         *
         * @var string
         */
        public $tailColor = '#000000';

        /**
         * Like to be Groomed ?
         * @var bool
         */
        public $likeToBeGroomed = true;

        /**
         * Is this dog like to sleep with a cushion ?
         * 
         * @var bool
         */
        public $sleepWithACushion = false;

        /**
         * Is dog Playing ?
         * @var bool
         */
        private $playing = false;

        /**
         * Bernese constructor.
         * @param array $params
         */
        public function __construct(array $params)
        {
            parent::__construct($params);
        }

        /**
         * play with big dog !
         */
        public function playWith() {
            
            $this->playing = true;
        }




    }