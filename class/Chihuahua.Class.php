<?php

    /**
     * Class Chihuahua
     */
    class Chihuahua extends Dog {


        /**
         * Has straight Ears.
         * @var bool
         *
         */
        public $hasStraightHears = true;

        /**
         * Is Excited.
         * Calm with $this->pet()
         *
         * @var bool
         */
        private $isExcited = true;


        /**
         * Chihuahua constructor.
         * @param array $params
         */
        public function __construct(array $params) {
            parent::__construct($params);
        }


        /**
         * Get Is Exited Value
         * @return bool
         */
        public function isExcited() {
            return $this->isExcited;
        }


        /**
         * Calm the Chihuahua by petting him
         */
        public function pet() {

            $this->isExcited = false;
        }

    }